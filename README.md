# ANI-M5Lightmeter

Lightmeter for (mostly) analog cameras based on a M5Stack and BH1750 light sensor

Prototype Page 1 | Page 2
---------------- |----------
![Prototype page 1](misc/m5lightmeter_page_1.jpg) |![page 2](misc/m5lightmeter_page_2.jpg)
The cable connects the BH1750 light sensor to the I2C bus

What you need
-
* M5Stack device ([M5Stack](https://m5stack.com/))
* Power supply with USB-C-Connector (e.g. USB power bank)
  * The M5Stack includes a small rechargeable battery that powers the M5Stack for about 40 Minutes of continuous usage
  * This capacity can be increased with the M5 Battery Module
* USB cable to upload the program
* [Arduino IDE](https://www.arduino.cc/en/Main/Software)
* [ESP32 board manager](https://dl.espressif.com/dl/package_esp32_index.json).
* BH1750 light sensor
* Cables for I2C connection

What to do
-
* Connect the M5Stack device via USB cable to your computer
  * Usually Windows will detect the device and assign it a COM port (e.g. COM3)
  * You don't need a special driver software for the CP2102 on Windows 10
* Install the Arduino IDE and the ESP32 board manager into the IDE
* Install the BH1750 and the M5Stack library
* Select via "Tools" the Board "M5Stack-Core-ESP32"
  * Select the right COM port
* Load the file ANI_M5Lightmeter.ino into the IDE
* Compile the file ANI_M5Lightmeter.ino in the Arduino IDE and upload it to your M5Stack device simply by pressing CTRL-U in the IDE
  * Sometimes you had to adjust the upload speed (in "Tools" in the IDEs menu)
* Connect the BH1750 to the I2C interface of the M5Stack:

BH1750 | M5Stack
-------|--------
VCC    | 3v3
GND    | GND
SCL    | SCL
SDA    | SDA

Usage
-
* The device is started by pressing the red button on the left side
* It can be switched off by double clicking the red button
* Metering is initially in ambient mode and continuous
* To preserve battery power the device shuts off after 60 seconds
* In the top left corner the EV is visible, top right corner shows the lux value
* Top center is the mode indicator "Ambient" or "FLASH"
* The table shows the ISO values on the x-axis and the aperture on the y-axis*
* The calculated shutter speeds are shown in the table
* Orange color indicates risk of camera shake
* The left button (button A) switches between ambient light metering and flash metering
  * In flash mode the light is sampled for 5 seconds showing the maximum EV
* The middle button (button B) stops or continues the metering
* The right button (button C) switches between the higher ISO page and the page with lower ISOs
* Pressing the right button for more than 3 seconds turns off/On the 60 seconds automatic shutdown
  * To disable auto shutdown completely change the the following code line 
	* const uint16_t initialAutoShutdownTime = 60;
	* to
	* const uint16_t initialAutoShutdownTime = -1;

Application|
-----------|
![On Canon AE-1](misc/m5lightmeter_on_camera.jpg)|
In front of the M5Stack: BH1750 light sensor|
Mounting: Manfrotto MCLAMP and 1/4 Zoll Tripod Screw to Flash Hot Shoe Mount Adapter|

License
-
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

Acknowledgment 
-
This project is based on the works of Vasyl Pominchuk (https://github.com/vpominchuk/lightmeter)
