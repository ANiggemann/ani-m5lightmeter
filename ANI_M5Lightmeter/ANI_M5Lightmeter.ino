/*
--------------------------------------------------
  ANI M5Lightmeter by Andreas Niggemann, 2020
  Versions:
  20201110 Base
  20201111 Switch off auto shutdown via long press button C
--------------------------------------------------
*/

#include <M5Stack.h>
#include <SPI.h>
#include <Wire.h>
#include <BH1750.h>
#include <Free_Fonts.h>
#include <math.h>

BH1750 lightMeter;

// Constants
const String ISO[] = {"50", "100", "200", "400", "800", "1600", "3200", "6400", "12800", "25600"};
const String apertures[] = {"1.4", "2", "2.8", "4", "5.6", "8", "11", "16", "22", "32"};
const String shutterSpeeds[] = {
  "8192'", "4096'",  "2048'",  "1024'",  "512'",   "256'",  "128'",
  "64'",   "32'",    "16'",    "8'",     "4'",     "2'",    "60",
  "30",    "15",     "8",      "4",      "2",      "1",     "1/2",
  "1/4",   "1/8",    "1/15",   "1/30",   "1/60",   "1/125", "1/250",
  "1/500", "1/1000", "1/2000", "1/4000", "1/8000", "-1",
  "-1",    "-1",     "-1",     "-1",     "-1",     "-1",
  "-1",    "-1",     "-1",     "-1",     "-1",     "-1"
};
const String cameraShakeWarningShutterSpeed = "1/60";
const uint16_t maxFlashMeteringTime = 5000;
const uint16_t initialAutoShutdownTime = 60;

// Variables
uint16_t autoShutdownTime = initialAutoShutdownTime;
float shutterSpeedValues[100];
unsigned long inactivityCounter = 0;
unsigned long previousMilliseconds = millis();
float lux;
float actuEV;
float previousEV;
boolean Overflow = 0;
uint8_t meteringMode = 1; // 1 = Ambient, 2 = Flash, -1 = Ambient Hold
uint8_t isoPage = 0;
uint16_t numberOfShutterSpeeds = 0;
uint16_t shutterSpeedBaseIndex = -1;
uint16_t cameraShakeWarningIndex = -1;

// Configuration
void setup() {
  M5.begin();
  M5.Lcd.setFreeFont(FSS9);
  Wire.begin();
  lightMeter.begin(BH1750::ONE_TIME_HIGH_RES_MODE_2);
  fillShutterSpeedValues();
  lux = getLux();
  processKeyboard();
}

// Main
void loop() {
  processKeyboard();
  if (autoShutdownTime > 0)
    checkInactivity();
  refresh();
  processMeteringModes();
}

void processMeteringModes() {
  if (meteringMode == 1) { // Ambient light meter mode.
    lightMeter.configure(BH1750::ONE_TIME_HIGH_RES_MODE_2);
    lux = getLux();
    if (Overflow == 1) {
      delay(10);
      getLux();
    }
    delay(200);
  } else if (meteringMode == 2) { // Flash light metering
    lightMeter.configure(BH1750::CONTINUOUS_LOW_RES_MODE);
    unsigned long startTime = millis();
    uint16_t currentLux = 0;
    lux = 0;
    while (true) { // check max flash metering time
      if (startTime + maxFlashMeteringTime < millis())
        break;
      currentLux = getLux();
      delay(32);
      if (currentLux > lux)
        lux = currentLux;
      processKeyboard();
      if (abs(meteringMode) == 1) // Key pressed to change to Ambient
        break;
    }
  }
}

void displayAll(float EValue) {
  if ((String(EValue) != "inf") && (EValue > -100) && (EValue < 1000)) {
    M5.Lcd.fillScreen(TFT_BLACK);
    M5.Lcd.setTextSize(1);

    M5.Lcd.setCursor(130, 15);
    M5.Lcd.setTextColor(TFT_YELLOW);
    M5.Lcd.print((abs(meteringMode) == 2) ? "FLASH" : "Ambient");

    String s = "Lux: " + String(lux);
    M5.Lcd.setCursor(318 - M5.Lcd.textWidth(s), 15);
    M5.Lcd.setTextColor(TFT_RED);
    M5.Lcd.print(s);
    M5.Lcd.setCursor(5, 15);
    M5.Lcd.setTextColor(TFT_WHITE);
    M5.Lcd.print("EV: " + String(floor(EValue + 0.5)));

    uint16_t tMaxWidth = M5.Lcd.textWidth("1/8000") + 4;
    uint16_t vStart = 32;
    float speed = getShutterSpeed(EValue, apertures[0].toFloat());
    shutterSpeedBaseIndex = findShutterSpeedBaseIndex(speed);
    for (uint8_t i = 0; i < 10; i++) {
      M5.Lcd.setCursor((vStart - 2 - M5.Lcd.textWidth(apertures[i])) / 2, 55 + (i * 20));
      M5.Lcd.setTextColor(TFT_WHITE);
      M5.Lcd.print(apertures[i]);
      M5.Lcd.drawFastHLine(0, i * 20 + 40, 320, TFT_GREEN);
    }
    for (uint8_t i = 0; i < 5; i++) {
      String isoString = ISO[i + (isoPage * 5)];
      uint16_t xPos = vStart + (i * tMaxWidth) + (tMaxWidth - M5.Lcd.textWidth(isoString)) / 2;
      M5.Lcd.setCursor(xPos, 35);
      M5.Lcd.setTextColor(TFT_WHITE);
      M5.Lcd.print(isoString);
      M5.Lcd.drawFastVLine(vStart + i * tMaxWidth, 40, 200, TFT_GREEN);
      for (uint8_t bi = 0; bi < 10; bi++) {
        uint16_t idx = shutterSpeedBaseIndex - bi + i - 1 + (isoPage * 5);
        String s = shutterSpeeds[idx];
        if (s != "-1") {
          uint16_t xtPos = vStart + i * tMaxWidth + (tMaxWidth - M5.Lcd.textWidth(s)) / 2;
          M5.Lcd.setCursor(xtPos, 55 + (bi * 20));
          uint16_t textColor = (idx < cameraShakeWarningIndex) ? TFT_ORANGE : TFT_WHITE;
          M5.Lcd.setTextColor(textColor);
          M5.Lcd.print(s);
        }
      }
    }
  }
}

void processKeyboard() {
  M5.update();
  if (M5.BtnA.isPressed() || M5.BtnB.isPressed() || M5.BtnC.isPressed()) {
    delay(200);
    inactivityCounter = 0;
    if (M5.BtnC.pressedFor(3000))
      autoShutdownTime = abs(autoShutdownTime - initialAutoShutdownTime);
    else {
      if (M5.BtnA.wasPressed())
        meteringMode = (abs(meteringMode) == 1) ? 2 : 1;
      else if (M5.BtnB.wasPressed())
        meteringMode = meteringMode * -1;
      else if (M5.BtnC.wasPressed())
        isoPage = !isoPage;
    }
    displayAll(actuEV);
  }
}

void checkInactivity() {
  unsigned long currentMilliseconds = millis();
  if ((currentMilliseconds - previousMilliseconds) > 1000) {
    inactivityCounter++;
    if (inactivityCounter > autoShutdownTime)
      M5.Power.powerOFF();
    previousMilliseconds = currentMilliseconds;
  }
}

float getLux() {
  Overflow = 0;
  uint16_t luxi = lightMeter.readLightLevel(false);
  if (luxi >= 65534) { // light sensor is overloaded
    Overflow = 1;
    luxi = 65535;
  }
  return luxi * 2.17;
}

float log2(float x) {
  return log(x) / log(2);
}

float lux2ev(float lux) {
  return log2(lux / 2.5);
}

void fillShutterSpeedValues() {
  float sSpeedValue = -1.0;
  numberOfShutterSpeeds = (sizeof(shutterSpeeds) / sizeof(shutterSpeeds[0]));
  for (uint16_t i = 0; i < numberOfShutterSpeeds; i++) {
    String s = shutterSpeeds[i];
    if (s == cameraShakeWarningShutterSpeed)
      cameraShakeWarningIndex = i;
    if (s.indexOf("'") > 0) {
      s = s.substring(0, s.length() - 1);
      sSpeedValue = s.toFloat() * 60;
    } else if (s.indexOf("/") > 0) {
      s = s.substring(2);
      sSpeedValue = 1 / s.toFloat();
    } else {
      sSpeedValue = s.toFloat();
    }
    shutterSpeedValues[i] = sSpeedValue;
  }
}

uint16_t findShutterSpeedBaseIndex(float speed) {
  uint16_t currentIndex = 0;
  for (uint16_t i = 0; i < numberOfShutterSpeeds; i++)
    if (abs(speed - shutterSpeedValues[i]) < abs(speed - shutterSpeedValues[currentIndex]))
      currentIndex = i;
  return currentIndex;
}

float getShutterSpeed(float ev, float aperture) {
  return (aperture * aperture) / pow(2, ev);
}

void refresh() {
  actuEV = round(lux2ev(lux) * 10) / 10;
  if (actuEV != previousEV) {
    previousEV = actuEV;
    displayAll(actuEV);
  }
}
